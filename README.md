# MandateThat
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/f91806042184452db4e107bc86d22a9a)](https://www.codacy.com/gl/lyonl/MandateThat?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=lyonl/MandateThat&amp;utm_campaign=Badge_Grade)
```csharp
Mandate.That(myString).IsNotNullOrWhiteSpace();
Mandate.That(myString, nameof(myArg)).IsNotNullOrWhiteSpace();
```
