﻿using System;

namespace MandateThat
{
    public static class MandateParamExtensions
    {
        /// <summary>   A MandateContext&lt;T&gt; extension method that is. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> Is<T>(this MandateContext<T> context, T comparisonValue, string message = default) where T : IComparable
        {
            Mandate.That(context, context.Value.CompareTo(comparisonValue) == 0, () =>
                new ArgumentOutOfRangeException(context.ParamName, context.Value,
                    message??$"Parameter must be equal to {context.Value}"));

            return new MandateContext<T>(context);
        }

        /// <summary>   A MandateContext&lt;T&gt; extension method that is between. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="minValue"> The minimum value. </param>
        /// <param name="maxValue"> The maximum value. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> IsBetween<T>(this MandateContext<T> context, T minValue, T maxValue, string message = default)
            where T : IComparable
        {
            Mandate.That(context,
                context.Value.CompareTo(minValue) != -1 && context.Value.CompareTo(maxValue) != 1,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value, message??
                        $"Parameter must be greater than or equal to {minValue} and less than or equal to {maxValue}"));

            return new MandateContext<T>(context);
        }

        /// <summary>   A MandateContext&lt;T&gt; extension method that is greater than. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> IsGreaterThan<T>(this MandateContext<T> context, T comparisonValue, string message = default)
            where T : IComparable
        {
            Mandate.That(context,
                context.Value.CompareTo(comparisonValue) == -1 || context.Value.CompareTo(comparisonValue) == 0,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value,message??
                        $"Parameter must be greater than or equal to {comparisonValue}"));


            return new MandateContext<T>(context);
        }

        /// <summary>
        /// A MandateContext&lt;T&gt; extension method that is greater than or equal to.
        /// </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> IsGreaterThanOrEqualTo<T>(this MandateContext<T> context, T comparisonValue, string message = default)
            where T : IComparable
        {
            Mandate.That(context, context.Value.CompareTo(comparisonValue) == -1,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value,message??
                        $"Parameter must be greater than {comparisonValue}"));

            return new MandateContext<T>(context);
        }

        /// <summary>
        /// A MandateContext&lt;T&gt; extension method that is less than or equal to.
        /// </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> IsLessThanOrEqualTo<T>(this MandateContext<T> context, T comparisonValue, string message = default)
            where T : IComparable
        {
            var val = context.Value.CompareTo(comparisonValue);

            Mandate.That(context, context.Value.CompareTo(comparisonValue) == 0,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value,message??
                        $"Parameter must be less than {comparisonValue}"));


            return context;
        }

        /// <summary>   A MandateContext&lt;T&gt; extension method that is less then. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> IsLessThen<T>(this MandateContext<T> context, T comparisonValue, string message = default)
            where T : IComparable
        {
            Mandate.That(context,
                context.Value.CompareTo(comparisonValue) == 1 || context.Value.CompareTo(comparisonValue) == 0,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value,message??
                        $"Parameter must be less than or equal to {comparisonValue}"));

            return context;
        }

        /// <summary>   A MandateContext&lt;T&gt; extension method that is not. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> IsNot<T>(this MandateContext<T> context, T comparisonValue, string message = default)
            where T : IComparable
        {
            Mandate.That(context, context.Value.CompareTo(comparisonValue) != 0, () =>
                new ArgumentOutOfRangeException(context.ParamName, context.Value, message??
                    $"Parameter must not be equal to {comparisonValue}"));

            return new MandateContext<T>(context);
        }

        /// <summary>   A MandateContext&lt;T&gt; extension method that is not between. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="minValue"> The minimum value. </param>
        /// <param name="maxValue"> The maximum value. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> IsNotBetween<T>(this MandateContext<T> context, T minValue, T maxValue, string message = default)
            where T : IComparable
        {
            Mandate.That(context,
                context.Value.CompareTo(minValue) == 1 && context.Value.CompareTo(maxValue) == -1,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value,message??
                        $"Parameter must be greater than {minValue} and less than {maxValue}"));

            return new MandateContext<T>(context);
        }

        /// <summary>   A MandateContext&lt;T&gt; extension method that is not null. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> IsNotNull<T>(this MandateContext<T> context, string message = default) where T : class
        {
            Mandate.That(context, context.Value == default(T), () => new ArgumentNullException(context.ParamName, message));


            return new MandateContext<T>(context);
        }

        /// <summary>   A MandateContext&lt;T&gt; extension method that is null. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A MandateContext&lt;T&gt; </returns>

        public static MandateContext<T> IsNull<T>(this MandateContext<T> context, string message = default) where T : class
        {
            Mandate.That(context, context.Value != default(T), () => new ArgumentException(context.ParamName,message));

            return new MandateContext<T>(context);
        }
    }
}