﻿namespace MandateThat
{
    public sealed class BooleanContext : MandateContext<bool>
    {
        internal BooleanContext(BooleanContext parentContext) : base(parentContext)
        {
        }

        internal BooleanContext(string paramName, bool value) : base(paramName, value)
        {
        }
    }
}