﻿using System;

namespace MandateThat
{
    public static class DateTimeMandateExtensions
    {
        /// <summary>   A DateTimeContext extension method that is. </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value Date/Time. </param>
        /// <param name="message">          (Optional) The message. </param>
        ///
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext Is(this DateTimeContext context, DateTime comparisonValue,
            string message = default)
        {
            Mandate.That(context, context.Value != comparisonValue, () =>
                new ArgumentException(context.ParamName, message ??
                                                         $"Parameter must be equal to {context.Value}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is between. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="minValue"> The minimum value Date/Time. </param>
        /// <param name="maxValue"> The maximum value Date/Time. </param>
        /// <param name="message">  (Optional) The message. </param>
        ///
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsBetween(this DateTimeContext context, DateTime minValue, DateTime maxValue,
            string message = default)
        {
            Mandate.That(context,
                minValue < context.Value && maxValue < context.Value,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                        $"Parameter must be greater than or equal to {minValue} and less than or equal to {maxValue}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is maximum value. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        ///
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsMaxValue(this DateTimeContext context, string message = default)
        {
            Mandate.That(context, context.Value != DateTime.MaxValue, () =>
                new ArgumentOutOfRangeException(context.ParamName, context.Value,
                    message ?? $"Parameter must be equal to {DateTime.MaxValue}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is minimum value. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        ///
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsMinValue(this DateTimeContext context, string message = default)
        {
            Mandate.That(context, context.Value != DateTime.MinValue, () =>
                new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                    $"Parameter must be equal to {DateTime.MinValue}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is daylight saving time. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        ///
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsDaylightSavingTime(this DateTimeContext context, string message = default)
        {
            Mandate.That(context, context.Value.IsDaylightSavingTime(), () =>
                new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                    $"Parameter must be equal to {context.Value}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is greater than. </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value Date/Time. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsGreaterThan(this DateTimeContext context, DateTime comparisonValue,
            string message = default)
        {
            Mandate.That(context, context.Value <= comparisonValue,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                        $"Parameter must be greater than {comparisonValue}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is greater than or equal to. </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value Date/Time. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsGreaterThanOrEqualTo(this DateTimeContext context, DateTime comparisonValue,
            string message = default)
        {
            Mandate.That(context, context.Value < comparisonValue,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                        $"Parameter must be greater than or equal to {comparisonValue}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is less than or equal to. </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value Date/Time. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsLessThanOrEqualTo(this DateTimeContext context, DateTime comparisonValue,
            string message = default)

        {
            Mandate.That(context, context.Value > comparisonValue,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                        $"Parameter must be less than or equal to {comparisonValue}"));

            return context;
        }

        /// <summary>   A DateTimeContext extension method that is less then. </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value Date/Time. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsLessThen(this DateTimeContext context, DateTime comparisonValue,
            string message = default)
        {
            Mandate.That(context, context.Value >= comparisonValue,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                        $"Parameter must be less than {comparisonValue}"));

            return context;
        }

        /// <summary>   A DateTimeContext extension method that is not. </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value Date/Time. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsNot(this DateTimeContext context, DateTime comparisonValue,
            string message = default)
        {
            Mandate.That(context, context.Value != comparisonValue, () =>
                new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                    $"Parameter must not be equal to {comparisonValue}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is not between. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="minValue"> The minimum value Date/Time. </param>
        /// <param name="maxValue"> The maximum value Date/Time. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsNotBetween(this DateTimeContext context, DateTime minValue, DateTime maxValue,
            string message = default)
        {
            Mandate.That(context,
                context.Value.CompareTo(minValue) == 1 && context.Value.CompareTo(maxValue) == -1,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                        $"Parameter must be greater than {minValue} and less than {maxValue}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is not maximum value. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsNotMaxValue(this DateTimeContext context, string message = default)
        {
            Mandate.That(context, context.Value == DateTime.MaxValue, () =>
                new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                    $"Parameter must not be equal to {DateTime.MaxValue}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is not minimum value. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsNotMinValue(this DateTimeContext context, string message = default)
        {
            Mandate.That(context, context.Value == DateTime.MinValue, () =>
                new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                    $"Parameter must not be equal to {DateTime.MinValue}"));

            return new DateTimeContext(context);
        }

        /// <summary>   A DateTimeContext extension method that is not daylight saving time. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A DateTimeContext. </returns>
        public static DateTimeContext IsNotDaylightSavingTime(this DateTimeContext context, string message = default)
        {
            Mandate.That(context, !context.Value.IsDaylightSavingTime(), () =>
                new ArgumentOutOfRangeException(context.ParamName, context.Value, message ??
                    $"Parameter must be equal to {context.Value}"));

            return new DateTimeContext(context);
        }
    }
}