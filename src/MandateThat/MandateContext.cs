﻿namespace MandateThat
{
    public class MandateContext<T>
    {
        protected internal MandateContext(MandateContext<T> parentContext) : this(parentContext.ParamName,
            parentContext.Value)
        {
            ParentContext = parentContext;
            ParentContext.HasChild = true;
        }

        protected internal MandateContext(string paramName, T value)
        {
            ParamName = paramName;
            Value = value;
        }

        public bool HasChild { get; protected set; }
        public MandateContext<T> ParentContext { get; }

        public string ParamName { get; }
        public T Value { get; }
    }
}