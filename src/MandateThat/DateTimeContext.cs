﻿using System;

namespace MandateThat
{
    public sealed class DateTimeContext : MandateContext<DateTime>
    {
        internal DateTimeContext(MandateContext<DateTime> parentContext) : base(parentContext)
        {
        }

        internal DateTimeContext(string paramName, DateTime value) : base(paramName, value)
        {
        }
    }
}