﻿using System;

namespace MandateThat
{
    public static class BooleanMandateExtensions
    {
        /// <summary>   A BooleanContext extension method that is. </summary>
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  True to comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A BooleanContext. </returns>
        public static BooleanContext Is(this BooleanContext context, bool comparisonValue, string message = default)
        {
            Mandate.That(context, context.Value != comparisonValue,
                () => new ArgumentException(message, context.ParamName));

            return new BooleanContext(context);
        }

        /// <summary>   A BooleanContext extension method that is false. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A MandateContext&lt;bool&gt; </returns>

        public static MandateContext<bool> IsFalse(this BooleanContext context, string message = default)
        {
            context.Is(false, message);

            return new BooleanContext(context);
        }

        /// <summary>   A BooleanContext extension method that is true. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A MandateContext&lt;bool&gt; </returns>

        public static MandateContext<bool> IsTrue(this BooleanContext context, string message = default)
        {
            context.Is(true, message);

            return new BooleanContext(context);
        }
    }
}