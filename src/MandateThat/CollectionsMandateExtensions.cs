﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MandateThat
{
    public static class CollectionsMandateExtensions
    {
        /// <summary>
        /// A MandateContext&lt;IEnumerable&lt;T&gt;&gt; extension method that determines if this
        /// collection contains a given object.
        /// </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="value">    The value. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A MandateContext&lt;IEnumerable&lt;T&gt;&gt; </returns>

        public static MandateContext<IEnumerable<T>> Contains<T>(this MandateContext<IEnumerable<T>> context, T value, string message = default)
        {
            context.IsNotNull(message);

            Mandate.That(context, !context.Value.Contains(value), () => new ArgumentException(context.ParamName, message));

            return new MandateContext<IEnumerable<T>>(context);
        }

        /// <summary>
        /// A MandateContext&lt;IEnumerable&lt;T&gt;&gt; extension method that determines if this
        /// collection contains a given object.
        /// </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="value">    The value. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A MandateContext&lt;IEnumerable&lt;T&gt;&gt; </returns>

        public static MandateContext<T[]> Contains<T>(this MandateContext<T[]> context, T value, string message = default)
        {
            context.IsNotNull(message);

            Mandate.That(context, !context.Value.Contains(value), () => new ArgumentException(context.ParamName, message));

            return new MandateContext<T[]>(context);
        }

        /// <summary>
        /// A MandateContext&lt;IEnumerable&lt;T&gt;&gt; extension method that determines if this
        /// collection contains a given object.
        /// </summary>
        ///
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="value">    The value. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A MandateContext&lt;IEnumerable&lt;T&gt;&gt; </returns>

        public static MandateContext<IDictionary<TKey, TValue>> Contains<TKey, TValue>(
            this MandateContext<IDictionary<TKey, TValue>> context, KeyValuePair<TKey, TValue> value, string message = default)
        {
            context.IsNotNull(message);


            Mandate.That(context, context.Value.Contains(value), () => new ArgumentException(context.ParamName, message));

            return new MandateContext<IDictionary<TKey, TValue>>(context);
        }

        /// <summary>
        /// A MandateContext&lt;IEnumerable&lt;T&gt;&gt; extension method that has any.
        /// </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A MandateContext&lt;IEnumerable&lt;T&gt;&gt; </returns>

        public static MandateContext<IEnumerable<T>> HasAny<T>(this MandateContext<IEnumerable<T>> context, string message = default)
        {
            context.IsNotNull(message);

            Mandate.That(context, !context.Value.Any(), () => new ArgumentException(context.ParamName, message));

            return new MandateContext<IEnumerable<T>>(context);
        }

        /// <summary>
        /// A MandateContext&lt;IEnumerable&lt;T&gt;&gt; extension method that has any.
        /// </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A MandateContext&lt;IEnumerable&lt;T&gt;&gt; </returns>

        public static MandateContext<T[]> HasAny<T>(this MandateContext<T[]> context, string message = default)
        {
            context.IsNotNull(message);

            Mandate.That(context, !context.Value.Any(), () => new ArgumentException(context.ParamName, message));

            return new MandateContext<T[]>(context);
        }

        /// <summary>
        /// A MandateContext&lt;IEnumerable&lt;T&gt;&gt; extension method that has any.
        /// </summary>
        ///
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A MandateContext&lt;IEnumerable&lt;T&gt;&gt; </returns>

        public static MandateContext<IDictionary<TKey, TValue>> HasAny<TKey, TValue>(
            this MandateContext<IDictionary<TKey, TValue>> context, string message = default)
        {
            context.IsNotNull(message);


            Mandate.That(context, context.Value.Count == 0, () => new ArgumentException(context.ParamName, message));

            return new MandateContext<IDictionary<TKey, TValue>>(context);
        }

        /// <summary>
        /// A MandateContext&lt;IEnumerable&lt;T&gt;&gt; extension method that size is.
        /// </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="expected"> The expected. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;IEnumerable&lt;T&gt;&gt; </returns>

        public static MandateContext<IEnumerable<T>> SizeIs<T>(this MandateContext<IEnumerable<T>> context,
            int expected, string message = default)
        {
            context.IsNotNull(message);

            Mandate.That(context, context.Value.Count() != expected,
                () => new ArgumentOutOfRangeException(context.ParamName, message));

            return new MandateContext<IEnumerable<T>>(context);
        }

        /// <summary>
        /// A MandateContext&lt;IEnumerable&lt;T&gt;&gt; extension method that size is.
        /// </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="expected"> The expected. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;IEnumerable&lt;T&gt;&gt; </returns>

        public static MandateContext<T[]> SizeIs<T>(this MandateContext<T[]> context, int expected, string message = default)
        {
            context.IsNotNull(message);

            Mandate.That(context, context.Value.Length != expected, () => new ArgumentOutOfRangeException(context.ParamName, message));

            return new MandateContext<T[]>(context);
        }

        /// <summary>
        /// A MandateContext&lt;IEnumerable&lt;T&gt;&gt; extension method that size is.
        /// </summary>
        ///
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="context">  The context to act on. </param>
        /// <param name="expected"> The expected. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A MandateContext&lt;IEnumerable&lt;T&gt;&gt; </returns>

        public static MandateContext<IDictionary<TKey, TValue>> SizeIs<TKey, TValue>(
            this MandateContext<IDictionary<TKey, TValue>> context, int expected, string message = default)
        {
            context.IsNotNull(message);


            Mandate.That(context, context.Value.Count != expected, () => new ArgumentOutOfRangeException(context.ParamName, message));

            return new MandateContext<IDictionary<TKey, TValue>>(context);
        }
    }
}