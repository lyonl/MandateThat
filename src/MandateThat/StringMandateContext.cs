namespace MandateThat
{
    public sealed class StringMandateContext : MandateContext<string>
    {
        internal StringMandateContext(StringMandateContext parentContext) : base(parentContext)
        {
        }

        internal StringMandateContext(string paramName, string value) : base(paramName, value)
        {
        }
    }
}