using System;

namespace MandateThat
{
    public static class StringMandateContextExtensions
    {
        /// <summary>   A StringMandateContext extension method that is length greater than. </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsLengthGreaterThan(this StringMandateContext context, int comparisonValue, string message = default)
        {
            Mandate.That(context, context.Value.Length > comparisonValue,
                () => new ArgumentOutOfRangeException(context.ParamName, context.Value, message));

            return new StringMandateContext(context);
        }

        /// <summary>
        /// A StringMandateContext extension method that is length greater than or equal to.
        /// </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsLengthGreaterThanOrEqualTo(this StringMandateContext context,
            int comparisonValue, string message = default)
        {
            Mandate.That(context, context.Value.Length >= comparisonValue,
                () =>
                    new ArgumentOutOfRangeException(context.ParamName, context.Value, message));

            return new StringMandateContext(context);
        }

        /// <summary>   A StringMandateContext extension method that is length less than. </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsLengthLessThan(this StringMandateContext context, int comparisonValue, string message = default)
        {
            Mandate.That(context, context.Value.Length < comparisonValue,
                () => new ArgumentOutOfRangeException(context.ParamName, context.Value, message));

            return new StringMandateContext(context);
        }

        /// <summary>
        /// A StringMandateContext extension method that is length less than or equal to.
        /// </summary>
        ///
        /// <param name="context">          The context to act on. </param>
        /// <param name="comparisonValue">  The comparison value. </param>
        /// <param name="message">          (Optional) The message. </param>
        /// <exception cref="ArgumentOutOfRangeException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsLengthLessThanOrEqualTo(this StringMandateContext context,
            int comparisonValue, string message = default)

        {
            Mandate.That(context, context.Value.Length <= comparisonValue,
                () => new ArgumentOutOfRangeException(context.ParamName, context.Value, message));

            return new StringMandateContext(context);
        }

        /// <summary>
        /// A StringMandateContext extension method that is not null or white space.
        /// </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsNotNullOrWhiteSpace(this StringMandateContext context,
            string message = default)
        {
            Mandate.That(context, string.IsNullOrWhiteSpace(context.Value),
                () => new ArgumentException(message, context.ParamName));

            return new StringMandateContext(context);
        }

        /// <summary>   A StringMandateContext extension method that is not null or empty. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsNotNullOrEmpty(this StringMandateContext context, string message = default)
        {
            Mandate.That(context, string.IsNullOrEmpty(context.Value),
                () => new ArgumentException(message, context.ParamName));

            return new StringMandateContext(context);
        }

        /// <summary>   A StringMandateContext extension method that is empty. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsEmpty(this StringMandateContext context, string message = default)
        {
            Mandate.That(context, context.Value == string.Empty,
                () => new ArgumentException(message, context.ParamName));

            return new StringMandateContext(context);
        }

        /// <summary>   A StringMandateContext extension method that is not empty. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsNotEmpty(this StringMandateContext context, string message = default)
        {
            Mandate.That(context, context.Value != string.Empty,
                () => new ArgumentException(message, context.ParamName));

            return new StringMandateContext(context);
        }

        /// <summary>   A StringMandateContext extension method that is not null. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentNullException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsNotNull(this StringMandateContext context, string message = default)
        {
            Mandate.That(context, context.Value == null, () => new ArgumentNullException(context.ParamName, message));


            return new StringMandateContext(context);
        }

        /// <summary>   A StringMandateContext extension method that is null. </summary>
        ///
        /// <param name="context">  The context to act on. </param>
        /// <param name="message">  (Optional) The message. </param>
        /// <exception cref="ArgumentException"> </exception>
        /// <returns>   A StringMandateContext. </returns>

        public static StringMandateContext IsNull(this StringMandateContext context, string message = default)
        {
            Mandate.That(context, context.Value != null, () => new ArgumentException( context.ParamName,message));


            return new StringMandateContext(context);
        }
    }
}