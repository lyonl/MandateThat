﻿using System;

namespace MandateThat
{
    public static class Mandate
    {
        public static BooleanContext That(bool value, string paramName = default)
        {
            return new BooleanContext(paramName, value);
        }

        public static DateTimeContext That(DateTime value, string paramName = default)
        {
            return new DateTimeContext(paramName, value);
        }

        public static MandateContext<byte> That(byte value, string paramName = default)
        {
            return new MandateContext<byte>(paramName, value);
        }

        public static MandateContext<short> That(short value, string paramName = default)
        {
            return new MandateContext<short>(paramName, value);
        }

        public static MandateContext<ushort> That(ushort value, string paramName = default)
        {
            return new MandateContext<ushort>(paramName, value);
        }


        public static MandateContext<int> That(int value, string paramName = default)
        {
            return new MandateContext<int>(paramName, value);
        }

        public static MandateContext<uint> That(uint value, string paramName = default)
        {
            return new MandateContext<uint>(paramName, value);
        }

        public static MandateContext<long> That(long value, string paramName = default)
        {
            return new MandateContext<long>(paramName, value);
        }

        public static MandateContext<ulong> That(ulong value, string paramName = default)
        {
            return new MandateContext<ulong>(paramName, value);
        }

        public static MandateContext<decimal> That(decimal value, string paramName = default)
        {
            return new MandateContext<decimal>(paramName, value);
        }

        public static MandateContext<float> That(float value, string paramName = default)
        {
            return new MandateContext<float>(paramName, value);
        }

        public static MandateContext<T> That<T>(T value, string paramName = default) where T : class
        {
            return new MandateContext<T>(paramName, value);
        }

        public static StringMandateContext That(string value, string paramName = default)
        {
            return new StringMandateContext(paramName, value);
        }


        public static void That<T, TException>(MandateContext<T> context, bool condition, Func<TException> defer)
            where TException : Exception, new()
        {
            if (condition) throw defer.Invoke();
        }
    }
}