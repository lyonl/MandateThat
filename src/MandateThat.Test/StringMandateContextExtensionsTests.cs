﻿using System;
using Xunit;

namespace MandateThat.Test
{
    public class StringMandateContextExtensionsTests
    {
        [Fact]
        public void Mandate_IsNotNull()
        {
            Assert.Throws<ArgumentNullException>(() => Mandate.That(default(string), "Test").IsNotNull());
        }

        [Fact]
        public void Mandate_IsNotNullOrEmpty()
        {
            Assert.Throws<ArgumentException>(() => Mandate.That(string.Empty, "Test").IsNotNullOrEmpty());

            Assert.Throws<ArgumentException>(() => Mandate.That(default(string), "Test").IsNotNullOrEmpty());


            Assert.Throws<ArgumentException>(() => Mandate.That(default(string), "Test").IsNotNullOrEmpty());
        }

        [Fact]
        public void Mandate_IsNotNullOrWhiteSpace()
        {
            Assert.Throws<ArgumentException>(() =>
                Mandate.That(default(string), "Test").IsNotNullOrWhiteSpace());

            Assert.Throws<ArgumentException>(() =>
                Mandate.That(" ", "Test").IsNotNullOrWhiteSpace());

            Assert.Throws<ArgumentException>(() => Mandate.That(default(string), "Test").IsNotNullOrWhiteSpace());
        }


        [Fact]
        public void Mandate_IsNull()
        {
            Assert.Throws<ArgumentException>(() => Mandate.That(" ", "Test").IsNull());
        }

        [Fact]
        public void Mandate_String_IsEmpty()
        {
            Assert.Throws<ArgumentException>(
                () => Mandate.That(string.Empty, "Test").IsEmpty());
        }

        [Fact]
        public void Mandate_String_IsLengthGreaterThan()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That("12", "Test").IsLengthGreaterThan(1));
        }

        [Fact]
        public void Mandate_String_IsLengthGreaterThanOrEqualTo()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                Mandate.That("12", "Test").IsLengthGreaterThanOrEqualTo(1));

            Assert.Throws<ArgumentOutOfRangeException>(() =>
                Mandate.That("12", "Test").IsLengthGreaterThanOrEqualTo(1));
        }


        [Fact]
        public void Mandate_String_IsLengthLessThan()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That("", "Test").IsLengthLessThan(1));
        }

        [Fact]
        public void Mandate_String_IsLessThanOrEqualTo()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                Mandate.That("1", "Test").IsLengthLessThanOrEqualTo(2));

            Assert.Throws<ArgumentOutOfRangeException>(
                () => Mandate.That("12", "Test").IsLengthLessThanOrEqualTo(2));
        }

        [Fact]
        public void Mandate_String_IsNotEmpty()
        {
            Assert.Throws<ArgumentException>(
                () => Mandate.That("1", "Test").IsNotEmpty());
        }

        [Fact]
        public void Mandate_string_IsNull_throw_if_string_is_not_null()
        {
            //Arrange
            var testString = "test";

            //Act
            void Actual()
            {
                Mandate.That(testString, "Test").IsNull();
            }

            //Assert
            Assert.Throws<ArgumentException>(Actual);
        }

        [Fact]
        public void Mandate_string_IsNull()
        {
            //Arrange
            string testString = null;

            //Act
            Mandate.That(testString, "Test").IsNull();
        }

        [Fact]
        public void Mandate_string_IsNotNull_throw_if_string_is_null()
        {
            //Arrange
            string testString = null;

            //Act
            void Actual()
            {
                Mandate.That(testString, "Test").IsNotNull();
            }

            //Assert
            Assert.Throws<ArgumentNullException>(Actual);
        }

        [Fact]
        public void Mandate_string_IsNotNull()
        {
            //Arrange
            var testString = "test";

            //Act
            Mandate.That(testString, "Test").IsNotNull();
        }
    }
}