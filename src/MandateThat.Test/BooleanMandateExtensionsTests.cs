﻿using System;
using Xunit;

namespace MandateThat.Test
{
    public class BooleanMandateExtensionsTests
    {
        [Fact]
        public void Mandate_Is()
        {
            Assert.Throws<ArgumentException>(() => Mandate.That(true, "Test").Is(false));
        }


        [Fact]
        public void Mandate_IsFalse()
        {
            Assert.Throws<ArgumentException>(() => Mandate.That(true, "Test").IsFalse());
        }

        [Fact]
        public void Mandate_IsTrue()
        {
            Assert.Throws<ArgumentException>(() => Mandate.That(false, "Test").IsTrue());
        }
    }
}