﻿using System;
using Xunit;

namespace MandateThat.Test
{
    public class DateTimeMandateContextExtensionsTests
    {
        [Fact]
        public void Mandate_Is()
        {
            Assert.Throws<ArgumentException>(() => Mandate.That(DateTime.MaxValue, "Test").Is(DateTime.MinValue));
        }

        [Fact]
        public void Mandate_IsBetween()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(new DateTime(1979, 12, 12), "Test")
                        .IsBetween(DateTime.MinValue, DateTime.MinValue.AddYears(1)));
        }

        [Fact]
        public void Mandate_IsGreaterThan()
        {
            var exception =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(DateTime.MinValue, "Test").IsGreaterThan(DateTime.MaxValue));

            Assert.Equal("Test", exception.ParamName);
            Assert.Equal(DateTime.MinValue, exception.ActualValue);

            var exception2 =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(DateTime.MinValue, "Test").IsGreaterThan(DateTime.MinValue));

            Assert.Equal("Test", exception2.ParamName);
            Assert.Equal(DateTime.MinValue, exception2.ActualValue);

            var result = Mandate.That(DateTime.MaxValue, "Test").IsGreaterThan(DateTime.MinValue);

            Assert.Equal("Test", result.ParamName);
            Assert.Equal(DateTime.MaxValue, result.Value);
        }

        [Fact]
        public void Mandate_IsGreaterThanOrEqualTo()
        {
            var exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
                Mandate.That(DateTime.MinValue, "Test").IsGreaterThanOrEqualTo(DateTime.MaxValue));

            Assert.Equal("Test", exception.ParamName);
            Assert.Equal(DateTime.MinValue, exception.ActualValue);

            var result = Mandate.That(DateTime.MinValue, "Test").IsGreaterThanOrEqualTo(DateTime.MinValue);

            Assert.Equal("Test", result.ParamName);
            Assert.Equal(DateTime.MinValue, result.Value);

            var result2 = Mandate.That(DateTime.MaxValue, "Test").IsGreaterThanOrEqualTo(DateTime.MinValue);

            Assert.Equal("Test", result2.ParamName);
            Assert.Equal(DateTime.MaxValue, result2.Value);
        }


        [Fact]
        public void Mandate_IsLessThanOrEqualTo()
        {
            var exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
                Mandate.That(DateTime.MaxValue, "Test").IsLessThanOrEqualTo(DateTime.MinValue));

            Assert.Equal("Test", exception.ParamName);
            Assert.Equal(DateTime.MaxValue, exception.ActualValue);


            var result2 = Mandate.That(DateTime.MinValue, "Test").IsLessThanOrEqualTo(DateTime.MinValue);

            Assert.Equal("Test", result2.ParamName);
            Assert.Equal(DateTime.MinValue, result2.Value);
        }

        [Fact]
        public void Mandate_IsLessThen()
        {
            var exception =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(DateTime.MaxValue, "Test").IsLessThen(DateTime.MinValue));

            Assert.Equal("Test", exception.ParamName);
            Assert.Equal(DateTime.MaxValue, exception.ActualValue);

            var result = Mandate.That(DateTime.MaxValue, "Test").IsGreaterThanOrEqualTo(DateTime.MinValue);

            Assert.Equal("Test", result.ParamName);
            Assert.Equal(DateTime.MaxValue, result.Value);

            var result2 = Mandate.That(DateTime.MaxValue, "Test").IsGreaterThanOrEqualTo(DateTime.MaxValue);

            Assert.Equal("Test", result2.ParamName);
            Assert.Equal(DateTime.MaxValue, result2.Value);
        }

        [Fact]
        public void Mandate_IsMaxValue()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That(DateTime.MinValue, "Test").IsMaxValue());
        }

        [Fact]
        public void Mandate_IsMinValue()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That(DateTime.MaxValue, "Test").IsMinValue());
        }

        [Fact]
        public void Mandate_IsNot()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(DateTime.MinValue, "Test").IsNot(DateTime.MaxValue));
        }

        [Fact]
        public void Mandate_IsNotMaxValue()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(
                    () => Mandate.That(DateTime.MaxValue, "Test").IsNotMaxValue());
        }

        [Fact]
        public void Mandate_IsNotMinValue()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(
                    () => Mandate.That(DateTime.MinValue, "Test").IsNotMinValue());
        }
    }
}