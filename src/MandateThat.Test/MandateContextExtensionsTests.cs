using System;
using MandateThat.Test.Test;
using Xunit;

namespace MandateThat.Test
{
    public class MandateContextExtensionsTests
    {
        [Fact]
        public void Mandate_Is()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((byte) 1, "Test").Is((byte) 1));
            Exception ex2 =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((short) 1, "Test").Is((short) 1));
            Exception ex3 =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((ushort) 1, "Test").Is((ushort) 1));
            Exception ex4 = Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That(1, "Test").Is(1));
            Exception ex5 =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((uint) 1, "Test").Is((uint) 1));
            Exception ex6 = Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That(1L, "Test").Is(1L));
            Exception ex7 =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((ulong) 1, "Test").Is((ulong) 1));
            Exception ex8 = Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That("1234", "Test").Is("1234"));
        }

        [Fact]
        public void Mandate_IsBetween()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(2, "Test").IsBetween(1, 3));
        }

        [Fact]
        public void Mandate_IsGreaterThan()
        {
            var exception =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(int.MinValue, "Test").IsGreaterThan(int.MaxValue));

            Assert.Equal("Test", exception.ParamName);
            Assert.Equal(int.MinValue, exception.ActualValue);

            var exception2 =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(int.MinValue, "Test").IsGreaterThan(int.MinValue));

            Assert.Equal("Test", exception2.ParamName);
            Assert.Equal(int.MinValue, exception2.ActualValue);

            var result = Mandate.That(int.MaxValue, "Test").IsGreaterThan(int.MinValue);

            Assert.Equal("Test", result.ParamName);
            Assert.Equal(int.MaxValue, result.Value);
        }

        [Fact]
        public void Mandate_IsGreaterThanOrEqualTo()
        {
            var exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
                Mandate.That(int.MinValue, "Test").IsGreaterThanOrEqualTo(int.MaxValue));

            Assert.Equal("Test", exception.ParamName);
            Assert.Equal(int.MinValue, exception.ActualValue);

            var result = Mandate.That(int.MinValue, "Test").IsGreaterThanOrEqualTo(int.MinValue);

            Assert.Equal("Test", result.ParamName);
            Assert.Equal(int.MinValue, result.Value);

            var result2 = Mandate.That(int.MaxValue, "Test").IsGreaterThanOrEqualTo(int.MinValue);

            Assert.Equal("Test", result2.ParamName);
            Assert.Equal(int.MaxValue, result2.Value);
        }

        [Fact]
        public void Mandate_IsLessThanOrEqualTo()
        {
            var exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
                Mandate.That(int.MinValue, "Test").IsLessThanOrEqualTo(int.MinValue));

            Assert.Equal("Test", exception.ParamName);
            Assert.Equal(int.MinValue, exception.ActualValue);


            var result2 = Mandate.That(int.MinValue, "Test").IsLessThanOrEqualTo(int.MaxValue);

            Assert.Equal("Test", result2.ParamName);
            Assert.Equal(int.MinValue, result2.Value);
        }

        [Fact]
        public void Mandate_IsLessThen()
        {
            var exception =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(int.MaxValue, "Test").IsLessThen(int.MinValue));

            Assert.Equal("Test", exception.ParamName);
            Assert.Equal(int.MaxValue, exception.ActualValue);

            var exception2 =
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    Mandate.That(int.MinValue, "Test").IsLessThen(int.MinValue));

            Assert.Equal("Test", exception2.ParamName);
            Assert.Equal(int.MinValue, exception2.ActualValue);


            var result = Mandate.That(int.MinValue, "Test").IsLessThen(int.MaxValue);

            Assert.Equal("Test", result.ParamName);
            Assert.Equal(int.MinValue, result.Value);
        }

        [Fact]
        public void Mandate_IsNot()
        {
            Exception ex =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((byte) 2, "Test").IsNot((byte) 1));
            Exception ex2 =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((short) 2, "Test").IsNot((short) 1));
            Exception ex3 =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((ushort) 2, "Test").IsNot((ushort) 1));
            Exception ex4 = Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That(2, "Test").IsNot(1));
            Exception ex5 =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((uint) 2, "Test").IsNot((uint) 1));
            Exception ex6 = Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That(2L, "Test").IsNot(1L));
            Exception ex7 =
                Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That((ulong) 2, "Test").IsNot((ulong) 1));
            Exception ex8 = Assert.Throws<ArgumentOutOfRangeException>(() => Mandate.That("1234", "Test").IsNot("123"));
        }
         [Fact]
        public void Mandate_class_object_IsNotNull()
        {
            //Arrange
            var testObject = new TestClass();

            //Act
            Mandate.That(testObject, "Test").IsNotNull();
        }

        [Fact]
        public void Mandate_IsNotNull_throw_if_class_object_is_null()
        {
            //Arrange
            TestClass testObject = null;

            //Act
            void Actual()
            {
                Mandate.That(testObject, "Test").IsNotNull();
            }

            //Assert
            Assert.Throws<ArgumentNullException>(Actual);
        }
        
        [Fact]
        public void Mandate_class_object_IsNull()
        {
            //Arrange
            TestClass testObject = null;

            //Act
            Mandate.That(testObject, "Test").IsNull();
        }

        [Fact]
        public void Mandate_IsNull_throw_if_class_object_is_not_null()
        {
            //Arrange
            var testObject = new TestClass();

            //Act
            void Actual()
            {
                Mandate.That(testObject, "Test").IsNull();
            }

            //Assert
            Assert.Throws<ArgumentException>(Actual);
        }
        
        [Fact]
        public void Mandate_IsNotNull_throws_when_object_is_null()
        {
            //Arrange
            var testObject = default(object); //null

            //Act
            void Actual()
            {
                Mandate.That(testObject, "Test").IsNotNull();
            }

            //Assert
            Assert.Throws<ArgumentNullException>(Actual);
        }

        [Fact]
        public void Mandate_IsNotNull()
        {
            //Arrange
            var testObject = new object(); //null

            //Act
            Mandate.That(testObject).IsNotNull();
        }

        [Fact]
        public void Mandate_IsNull_throw_if_object_is_not_null()
        {
            //Arrange
            var testObject = new object(); //null

            //Act
            void Actual()
            {
                Mandate.That(testObject, "Test").IsNull();
            }

            //Assert
            Assert.Throws<ArgumentException>(Actual);
        }
    }
}